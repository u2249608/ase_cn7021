const app_apis = [
    {
        path: ' https://fakerapi.it/api/v1/persons?_quantity=10',
        name: 'members'
    },
    {
        path: ' https://fakerapi.it/api/v1/companies?_quantity=10',
        name: 'clients'
    },
    {
        path: '  https://fakerapi.it/api/v1/products?_quantity=10',
        name: 'products'
    },
    {
        path: 'https://fakerapi.it/api/v1/credit_cards?_quantity=10',
        name: 'credit'
    }
];

export default app_apis;
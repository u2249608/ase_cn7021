import React, {useState, useEffect} from 'react';
import fetchDataFromApi from '../../services/api/handlers/api-handler';

// components
import AdvancedTable from '../../components/advance-table';
import Layout from '../../components/layout';
import Loader from '../../components/Loader';

const Credit = ({auth}) => {
    const headers = [
        'Type',
        'Number',
        'Expiry',
        'Name',
    ]
    const [loading, setLoading] = useState(false);
    const [data, setData] = useState([]);
    const [isError, setIsError] = useState(false);
    const [error, setError] = useState('');

    const hydrateData = (data) => {
        return data.map((item) => {
            return {
                Type: item.type,
                Number: item.number,
                Expiry: item.expiration,
                Name: item.owner
            }
        })
    }

    useEffect(
        () => {
            setLoading(true);
            fetchDataFromApi('credit')
            .then(res => {
                const data = hydrateData(res.data.data);
                setData(data);
                setLoading(false);
            }).catch(err => {
                setLoading(false);
                setIsError(true);
                setError(err);
            });
        },
        []
    );

    

    return (
        <Layout auth={auth}>  
            { loading && <Loader  height={"80vh"} />}
            { isError && error}
            {
                data.length > 0 
                ?
                    <AdvancedTable
                        title={'Credit Card table'}
                        headers={headers} 
                        data={data} 
                    />
                : 
                !loading && `${data.length} records available`
            }
        </Layout>
    )
}

export default Credit;